package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UserBookController {
    @GetMapping("/{userId}/books")
    public String createBook(@PathVariable("userId") String userid) {
        return "The book for user " + userid;
    }

    @GetMapping("/segments/good")
    public String isGood() {
        return "path is good";
    }

    @GetMapping("/segments/{segment}")
    public String isSegment(@PathVariable String segment) {
        return "path is segment";
    }

    @GetMapping("/?/match")
    public String matchLongerCase() {
        return "match longer success";
    }

    @GetMapping("/wildcards/*")
    public String matchWildCase() {
        return "match wildcards anything";
    }

    @GetMapping("/*/wildcards")
    public String matchWildCaseBeMiddle() {
        return "match wildcards be middle";
    }

    @GetMapping("/wildcard/before/*/after")
    public String matchWildCaseBeIgnore() {
        return "match wildcards be ignore";
    }

    @GetMapping("/wildcard/before/*prefix")
    public String matchWildCaseBePrefix() {
        return "match wildcards be prefix";
    }

    @GetMapping("/wildcard/before/suffix*")
    public String matchWildCaseBeSuffix() {
        return "match wildcards be suffix";
    }

    @GetMapping("/**/before")
    public String matchWildCaseBeManyStar() {
        return "match wildcards be many star";
    }
    @GetMapping("/{username:[a-z0-9]+}")
    public String matchRegex() {
        return "match regex";
    }
    @GetMapping("")
    public String queryNullParameter(@RequestParam Integer bookid) {
        return "query string routing success";
    }
    @GetMapping("/books")
    public String requestForbiddenParameter(@RequestParam(required = false) Integer userid) {
        return "request Forbidden Parameter";
    }
}
