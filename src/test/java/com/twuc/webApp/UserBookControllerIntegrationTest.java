package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class UserBookControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Test
    public void should_return_user1_book() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 2"));
    }
    @Test
    public void should_return_user2_book() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/23/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 23"));
    }
    @Test
    public void should_ignore_lowercase_uppercase() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 2"));
    }
    @Test
    public void should_call_hardcode_path_first() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/segments/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("path is good"));
    }
    @Test
    public void should_match_longer_word() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/test/match"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
    @Test
    public void should_match_nothing() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users//match"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
    @Test
    public void should_wildcard() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
    @Test
    public void should_wildcard_be_middle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/anything/wildcards"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
    @Test
    public void should_wildcard_be_ignore() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcard/before//after"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
    @Test
    public void should_wildcard_be_prefix() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcard/before/someprefix"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
    @Test
    public void should_wildcard_be_suffix() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcard/before/suffixsome"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
    @Test
    public void should_wildcard_be_manystar() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcard wildcard/before"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
    @Test
    public void should_match_regex() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/asd"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
    @Test
    public void should_query_string_routing() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users?bookid=3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("query string routing success"));
    }
    @Test
    public void should_query_not_provide_in_uri() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
    @Test
    public void should_forbide_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/books?userid=3"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }


}
