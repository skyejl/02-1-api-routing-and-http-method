package com.twuc.webApp;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserBookControllerTest {
    @Test
    public void should_return_user_book() {
        String book =  new UserBookController().createBook("2");
        assertEquals("The book for user 2", book);
    }


}
